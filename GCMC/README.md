# 1. Explicit reservoir simulation
* grand-canonical simulation of explicit reservoir of NaCl with same parameters in both LAMMPS using CR-MC formalism and ESPResSo using RxMC formalism
* the raw input is $\Gamma \in \{ 10^{-4}, 2\cdot 10^{-5}, 10^{-5}, 2\cdot 10^{-6}, 10^{-6}\}$ in LJ units -> output is equilibrium concentration
* three different force-field setups:
  - a. *ideal*:  ideal gas
  - b. *screened*:  WCA + Debye-Hueckel with $l_B = 2\sigma, \kappa = 0.2 \sigma^{-1}, r_{cut} \approx 12 \sigma$
  - c. *full*:  WCA + P3M with $l_B = 2\sigma, \delta F = 10^{-4}$
* box size $L = 30\sigma$ (always $\geq 30$ particles in the box), simulation length: $\approx 5000\tau$

* `exmu.pdf` shows that LAMMPS and ESPResSo produce almost the same result for the ideal gas and the screened setup
* there are differences in the full setup, presumably because of the incorrect treatment of $k$-space calculations in LAMMPS:
  - in ESPResSo, $\Delta E$ in MC is calculated from full potential energies of system with $N$ and $N+1$ particles
  - in LAMMPS, CR-MC core just creates an *exclusion* for ionic pair, when it attempts deletion -- this means that ionic pair is excluded from Verlet lists, and loses short-range interactions, but the ions are still considered in $k$-space part of electrostatics
  - this creates a bias in acceptance probabilities, and leads to incorrect equilibrium concentrations, when long-range interactions are considered
  
# 2. Consistency of GC-MC simulations

* to test the consistency of grand-canonical simulations, we took the generated trajectories and measured chemical potentials in an independent auxiliary simulation
* in this auxiliary simulations, we did not use any dynamics, just loaded static frames, and use Widom insertion to measure $\mu^{ex}$, and we also know $\mu^{id}$ from the density
* then we have $\beta \mu_{\textrm{NaCl}} = \beta \mu_{\textrm{NaCl}}^{id} + \beta \mu_{\textrm{NaCl}}^{ex.}$ and also $\beta \mu_{\textrm{NaCl}} = \ln(\Gamma)$, hence $\log_{10}(\Gamma) = \log_{10}(e) \cdot \mu_{\textrm{NaCl}}$
* `gammas.pdf` shows precisely this correlation - for ESPResSo it almost sits on the $y=x$ line, as it should according to the above equation -> this means that the ESPResSo simulations are consistent; in LAMMPS there is a systematic shift
