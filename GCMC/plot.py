#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

setups = [
	'ideal',
	'screened',
	'full',
]
packages = [
	'LAMMPS',
	'ESPResSo',
]

colors = cycle([
	'red',
	'blue',
	'green',
])
lines = cycle([
	'solid',
	'dotted',
])
shapes = cycle([
	'o',
	'v',
])


plt.style.use('bmh')
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "sans-serif",
    "font.size": 11,
    "font.sans-serif": ["Helvetica"],
    "axes.facecolor": '#ffffff',
	})

# ex_mu vs. conc.
plt.figure()
plt.xlim(0.01, 1)
plt.ylim(-0.8,0.05)
plt.xscale('log')
plt.xlabel('$c_{\\textrm{NaCl}}\\ [\\textrm{mol/L}]$')
plt.ylabel('$\\beta \\mu^{\\textrm{ex.}}_{\\textrm{NaCl}}$')
for package in packages:
  linestyle = next(lines)
  shape = next(shapes)
  for setup in setups:
    color = next(colors)
    data = np.loadtxt('DATA/GCMC-NaCl_{:}_{:}.dat'.format(package, setup), skiprows = 0)
    plt.errorbar(
      data[:,1], 
      data[:,3], 
      yerr = data[:,4], 
      label = '{:s}. {:s}.'.format(package[:3], setup[:4]),
      marker = shape,
      markersize = 5,
      markerfacecolor = 'none',
      linestyle = linestyle,
      linewidth = 1.0,
      elinewidth = 1.0,
      color = color,
    )

plt.legend(loc = 'best', ncol = 2)
plt.savefig('exmu.pdf')



# gamma vs mu correlation
plt.figure()
xymin, xymax = -6.1, -3.9
plt.xlim(xymin, xymax)
plt.ylim(xymin, xymax)
plt.plot(
  [xymin, xymax], 
  [xymin, xymax], 
  markersize = 0,
  linestyle = 'solid',
  linewidth = 2.0,
  color = 'gray',
)
plt.xlabel('$-\\log_{10}(\\Gamma)$')
plt.ylabel('$\\log_{10}(e) \\Big( \\beta \\mu^{\\textrm{id.}}_{\\textrm{NaCl}} + \\beta \\mu^{\\textrm{ex.}}_{\\textrm{NaCl}} \\Big)$')
for package in packages:
  linestyle = next(lines)
  shape = next(shapes)
  color = next(colors)
  data = np.loadtxt('DATA/GCMC-NaCl_{:}-MU.dat'.format(package), skiprows = 0)
  plt.errorbar(
    data[:,0], 
    data[:,1], 
    yerr = data[:,2], 
    label = '{:s}.'.format(package[:3]),
    marker = shape,
    markersize = 5,
    markerfacecolor = 'none',
    linestyle = linestyle,
    linewidth = 1.0,
    elinewidth = 1.0,
    color = color,
  )

plt.legend(loc = 'best', title = 'full', ncol = 1)
plt.savefig('gammas.pdf')

exit()

