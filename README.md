* acid_polymer_with_res.py by Peter Košovan
    - weak acidic polymer coupled to a reservoir, simulated in the G-RxMC ensemble  - the main example script
    - runs with Espresso-4.1.4

* analyze-outputs.py
    - it analyzes the time-traces of observables computed by these scripts, 
    - it computes the following statistics for each observable: mean, standard error of the mean corrected for correlations, effective number of uncorrelated samples and correlation time; 
        - the estimated error and correlation times are reliable only if the effective number of uncorrelated samples is > 100
        - equilibration is excluded from the analysis (first 10% of the time-trace, if not specified otherwise)
    - it produces a csv file that contains parameters of each simulation extracted from the file name and statistics for each observable
    - runs with python3 + other modules that are imported by the script

* ideal_acid.py By Peter Košovan
    - simulation of an ideal acid in the RxMC ensemble to check that we are setting the values of K_A correctly
    - runs with Espresso-4.1.4

* reservoir.py By Peter Košovan
    - grandcanonical simulation of a reservoir of NaCl using the RxMC framework to check that we are setting the values of K_NaCl correctly
    - runs with Espresso-4.1.4

* weak-polymer_16_chains.py By Jonas Landsgesell
    - Minimal working example of grand reaction ensemble method for simulating a system in contact with a supernatant phase (e.g. separated by a membrane). 
    - This script should run out of the box with python 2.7 and pypresso compiled from the git commit 0edd93589dde87d1d104b1479bfa26e6b8889276 from the repository https://github.com/espressomd/espresso

* statistic.py By Jonas Landsgesell
    - (?) processing of the weak-polymer script
