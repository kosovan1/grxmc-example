#!/usr/bin/env python
# coding: utf-8

# designed to work with Espresso-4.1.4

# ionization degree alpha calculated from the Henderson-Hasselbalch equation for an ideal system
def calc_ideal_alpha(pH, pKa):
    return 1. / (1 + 10**(pKa - pH))

def calc_donnan_coefficient(c_acid, I_res, charge = -1):
    return charge*c_acid/(2*I_res) + np.sqrt( ( charge*c_acid/(2*I_res) )**2 + 1 )

def calc_gamma(ionic_strength, z=1):
    '''
    Activity coefficient of ionic solutions using the semi-empirical formula by Davies
    log10 (gamma) = - A * z**2 * ( sqrt(I) / (1 + sqrt(I) ) - 0.2*I )
    * I is the ionic strength in mol/L
    * A = 0.51 mol^{−1/2}dm^{3/2}
    '''
    A = 0.51
    I = ionic_strength.to('mol/l').magnitude
    if ( I > 0.1 ):
        raise ValueError("this calculation of activity coefficients is valid only up to I=0.1, got I = {:.2f} M".format(I) )
    log10_gamma = - A * z**2 * ( np.sqrt(I) / (1 + np.sqrt(I) ) - 0.2*I )
    return 10**(log10_gamma)

def print_particles(system, exit = False):
    '''
    pretty-print selected properties of particles for quick and dirty debugging
    '''
    i = 0
    for p in system.part:
        print("i: ", i, "  pid: ", p.id, "  pos: ", p.pos,  "  q: ", p.q, "  bonds: ", p.bonds)
        i += 1
    if(exit):
        print('print_particles(): exit here ')
        sys.exit()

import os
import os.path
import sys
import numpy as np
import pint  # module for working with units and dimensions
import time

import espressomd
espressomd.assert_features(['WCA', 'ELECTROSTATICS'])
import espressomd.electrostatics
import espressomd.reaction_ensemble
import espressomd.polymer
from espressomd.interactions import HarmonicBond
from espressomd.interactions import FeneBond

ureg = pint.UnitRegistry()

# Important constants
TEMPERATURE = 298 * ureg.K
WATER_PERMITTIVITY = 78.5 # at 298 K
KT = TEMPERATURE * ureg.k_B
MOLE = 6.022e23 / ureg.mole
BJERRUM_LENGTH = (ureg.e**2 / (4 * ureg.pi * ureg.eps0 * WATER_PERMITTIVITY * KT)).to('nm')
C_REF = 1.0* ureg('mol/l') # reference concentration for the pKa value
pKw = 14


# Input parameters that should be varied
DEBUG = False
system_settings = "JL" # use TC for T. Curk et al. and JL for J. Landsgesell et al.
PURE_RESERVOIR = True
N_SALT_RES = 400 # desired number of salt ion pairs (used only when simulating pure reservoir)
pKa = 4
pH = 7
pSalt = 2
epsilon_reduced = 0.0 # value of epsilon in reduced units
# number of reaction samples per each pH value
t_max = 2.1e4 # 1e3 equilibration + 2e4 production
dt = 0.01

# system-specific parameters that are different between Curk et al and Landsgesell et al
if (system_settings == "TC"): # settings used by T. Curk et al, when comparing with Espresso
    N_CHAINS = 1
    CHAIN_LENGTH = 100
    C_ACID = None # in this setup, C_ACID will be calculated later
    bond_type = "harmonic"
    k_harm = 400 # stiffness constant of the harmonic bond
    r_0 = 1.0 # average bond length (approximately)
    PARTICLE_SIZE = 0.71 * ureg.nm # value used by Curk, Yuan and Luijten
elif (system_settings == "JL"): # settings used by J. Landsgesell et al
    N_CHAINS = 16
#    N_CHAINS = 3 # to ensure fast calculation with more chains for testing purposes
    CHAIN_LENGTH = 50
    C_ACID = 0.15*ureg('mol/l')
    bond_type = "fene"
    k_fene = 30
    r_max = 1.5 # maximum extension of the bond
    PARTICLE_SIZE = 0.355 * ureg.nm # value used by Landsgesell
else:
    raise ValueError("system_settings must be either 'TC' or 'JL', got " + system_settings + "\n")


# additional parameters used by us
ideal = False # if set to true, all interactions are turned off to simualte an ideal system
time_per_sample = 10
steps_per_round = int(time_per_sample / dt)
NUM_SAMPLES = int(t_max / time_per_sample)

if(ideal):
    id_flag = 1
    # Simulate an interacting system with steric repulsion (Warning: it will be slower than without WCA!)
    USE_WCA = False
    # Simulate an interacting system with electrostatics 
    USE_ELECTROSTATICS = False
    # By default, we use the Debye-Huckel potential because it allows short runtime of the tutorial
    # You can also use the P3M method for full electrostatics (Warning: it will be much slower than DH!)
    USE_P3M = False
else:
    id_flag = 0
    USE_WCA = True
    USE_ELECTROSTATICS = True
    USE_P3M = True

if USE_ELECTROSTATICS:
    assert USE_WCA, "Use electrostatics only with a short-range repulsive potential. Otherwise, singularity occurs at zero distance."

# if we specified C_ACID, then calculate BOX_V, otherwise, do the reverse
N_ACID = N_CHAINS * CHAIN_LENGTH
if (system_settings == "TC" and not PURE_RESERVOIR):
    # Curk et al
    BOX_L = 50*BJERRUM_LENGTH
    BOX_V = BOX_L**3
    C_ACID = N_ACID / MOLE / BOX_V
elif (system_settings == "JL" and not PURE_RESERVOIR):
    # Landsgesell
    BOX_V = N_ACID / MOLE / C_ACID 
    BOX_L = BOX_V**(1/3)
elif (PURE_RESERVOIR):
    print("set box volume here")
    BOX_V = N_SALT_RES / MOLE / ( 10**(-pSalt) * ureg('mol/l') )
    BOX_L = BOX_V**(1/3)
else:
    raise ValueError("system_settings must be either 'TC' or 'JL', got " + system_settings + "\n")

#C_ACID_REDUCED_CORRECT = N_ACID/BOX_V.to('reduced_length^3')
#print("1/C_REDUCED_CORRECT: ", 1/C_ACID_REDUCED_CORRECT)

if(PURE_RESERVOIR):
    method_name = "Res";
else:
    method_name = "GRxMC"
sysname ="{0:s}-{1:s}".format(method_name,system_settings) \
    + "_id-{:}".format(id_flag) \
    + "_N-{:.0f}".format(CHAIN_LENGTH) \
    + "_n-{:.0f}".format(N_CHAINS) \
    + "_cp-{:.4f}".format(C_ACID.to('mol/l').magnitude) \
    + "_pK-{:.1f}".format(pKa) \
    + "_pH-{:.1f}".format(pH) \
    + "_pS-{:.1f}".format(pSalt) \
    + "_e-{:.1f}".format(epsilon_reduced) \

print("sysname:", sysname)

basename = os.path.basename(__file__)
print("basename:", basename)
template_name = "acid_polymer_with_res.py"
new_name = sysname+".py"
if(basename == new_name):
    print("basename OK, run")
    pass
elif(basename == template_name):
    print("basename is template, create the script and run")
    if( os.path.exists(new_name) ):
            raise ValueError( "\n\tScript " + new_name + " already exists, please check and delete it if desired.\n" )
    os.system( 'cp {} {}'.format(template_name, new_name) )
    os.system( './pypresso {}'.format(new_name) )
    sys.exit()
else:
    raise ValueError("Unknown basename {}, should be either {} or {}".format(basename, sysname, template_name) )

# definitions of reduced units
ureg.define(f'reduced_energy = {TEMPERATURE} * boltzmann_constant')
ureg.define(f'reduced_length = {PARTICLE_SIZE}')
ureg.define(f'reduced_charge = 1*e')
KT_REDUCED = KT.to('reduced_energy').magnitude
BJERRUM_LENGTH_REDUCED = BJERRUM_LENGTH.to('reduced_length').magnitude
PARTICLE_SIZE_REDUCED = PARTICLE_SIZE.to('reduced_length').magnitude
C_REF_REDUCED = ( C_REF*MOLE ).to('reduced_length^-3')
print("KT = {:4.3f}".format(KT.to('reduced_energy')))
print("PARTICLE_SIZE = {:4.3f}".format(PARTICLE_SIZE.to('reduced_length')))
print("BJERRUM_LENGTH = {:4.3f}".format(BJERRUM_LENGTH.to('reduced_length')))

# calculate the dependent parameters
BOX_L_REDUCED = BOX_L.to('reduced_length').magnitude
print("Calculated values:")
print(f"BOX_L = {BOX_L:.3g} = {BOX_L.to('reduced_length'):.3g}")
print(f"BOX_V  = {BOX_V:.3g} = {BOX_V.to('reduced_length^3'):.3g} = {BOX_V.to('dm^3'):.3g}")
print(f"C_ACID = {C_ACID.to('mol/L'):.3g}")
print(f"N_ACID = {N_ACID}")


c_H = 10**(-pH) * ureg('mol/l')
c_OH = 10**(pH-pKw) * ureg ('mol/l')

c_neutralizer = np.abs(c_H - c_OH)
c_neutralizer_reduced = ( c_neutralizer * MOLE).to('reduced_length^-3')

c_salt = 10**(-pSalt) * ureg('mol/l')
c_salt_reduced = ( c_salt * MOLE ).to('reduced_length^-3')
if (c_H > c_OH):
    c_Cl = c_salt + c_neutralizer
    c_Na = c_salt 
else:
    c_Cl = c_salt 
    c_Na = c_salt + c_neutralizer

print("c_salt:", c_salt)

I_res1 = c_Na + c_H
I_res2 = c_Cl + c_OH
# check if I_res based on anions and cations are consistent
if ( np.abs(I_res1 - I_res2) / I_res1 < 1e-3):
    I_res = I_res1;
    print("I_res:", I_res)
else: 
    raise ValueError("Shoulbe be I_res1 = I_res2, got I_res[1,2]: {} {}\n".format(I_res1, I_res2) )

if(ideal):
    gamma_res = 1.0
else:
    gamma_res = calc_gamma(ionic_strength = I_res)
print("gamma_res:", gamma_res)

ideal_alpha = calc_ideal_alpha( pH=pH, pKa=pKa )
print("ideal_alpha:", ideal_alpha)
max_donnan_coefficient = calc_donnan_coefficient( C_ACID.to('mol/l'), I_res.to('mol/l') )
print("max_donnan:", max_donnan_coefficient)
print("log10(max_donnan):", np.log10(max_donnan_coefficient) )

# concentrations in reduced units N/(sigma**3)
c_H_reduced =  (c_H  * MOLE).to('reduced_length^-3')
c_OH_reduced = (c_OH * MOLE).to('reduced_length^-3')
c_Cl_reduced = (c_Cl * MOLE).to('reduced_length^-3')
c_Na_reduced = (c_Na * MOLE).to('reduced_length^-3')

# equilibrium constants for the insertion reactions
K_NaCl_reduced = (c_Na_reduced * c_Cl_reduced).magnitude * ( gamma_res**2 )
K_NaOH_reduced = (c_Na_reduced * c_OH_reduced).magnitude * ( gamma_res**2 )
K_HCl_reduced  = (c_H_reduced  * c_Cl_reduced).magnitude * ( gamma_res**2 )
K_w_reduced    = (c_H_reduced  * c_OH_reduced).magnitude * ( gamma_res**2 )

# equilibrium constants for the acid-base reactions
if (not PURE_RESERVOIR):
    K_A = 10**(-pKa) 
    K_A_reduced  = K_A * C_REF_REDUCED.magnitude 
    K_AOH_reduced  = K_A_reduced / K_w_reduced
    K_ANa_reduced  = K_A * C_REF_REDUCED.magnitude * (c_Na_reduced / c_H_reduced).magnitude
    K_ACl_reduced  = K_A * C_REF_REDUCED.magnitude / (c_Cl_reduced * c_H_reduced).magnitude

PROB_INTEGRATION = 1.0 # Probability of running MD integration after the reaction move. 
# This parameter changes the speed of convergence but not the limiting result
# to which the simulation converges

# #### Set the particle types and charges

# particle types of different species
TYPES = {
    "HA": 0,
    "A": 1,
    "H": 2,
    "Na": 3,
    "OH": 4,
    "Cl": 5,
}
# particle charges of different species
CHARGES = {
    "HA": ( 0 * ureg.e).to("reduced_charge").magnitude,
    "A" : (-1 * ureg.e).to("reduced_charge").magnitude,
    "H" : (+1 * ureg.e).to("reduced_charge").magnitude,
    "OH": (-1 * ureg.e).to("reduced_charge").magnitude,
    "Na": (+1 * ureg.e).to("reduced_charge").magnitude,
    "Cl": (-1 * ureg.e).to("reduced_charge").magnitude,
}


# ### Initialize the ESPResSo system
system = espressomd.System(box_l=[BOX_L_REDUCED] * 3)
system.time_step = dt
system.cell_system.skin = 2.0
np.random.seed(seed=10)  # initialize the random number generator in numpy
# make sure that all particle types are tracked
for p_type in TYPES:
    system.setup_type_map([TYPES[p_type]])

# ### Set up particles and bonded-interactions
if (not PURE_RESERVOIR):
    eq_bond = HarmonicBond(k=100, r_0=1.0) # this bond setup should be used for equilibration to prevent crashes of the integrator
    system.bonded_inter.add(eq_bond)
    if (system_settings == 'TC'):
        # we need to define bonds before creating polymers
        final_bond = HarmonicBond(k=k_harm, r_0=r_0) # 
    elif (system_settings == 'JL'):
        # we need to define bonds before creating polymers
        final_bond = FeneBond(k=k_fene, d_r_max = r_max) # 
    system.bonded_inter.add(final_bond)
    
    # create the polymer positions
    polymers = espressomd.polymer.linear_polymer_positions(n_polymers=N_CHAINS,
                                                           beads_per_chain=CHAIN_LENGTH,
                                                           bond_length=0.9, 
                                                           seed=23)
    
    # add the polymer particles composed of ionizable acid groups, initially in the ionized state
    for polymer in polymers:
        prev_particle = None
        for position in polymer:
            p = system.part.add(pos=position, type=TYPES["A"], q=CHARGES["A"])
            if prev_particle:
                p.add_bond((eq_bond, prev_particle))
            prev_particle = p
    
    # add the corresponding number of H+ ions
    system.part.add(pos=np.random.random((N_ACID, 3)) * BOX_L_REDUCED,
                    type=[TYPES["H"]] * N_ACID,
                    q=[CHARGES["H"]] * N_ACID)

if(DEBUG):
    print_particles(system)

## add salt ion pairs - initialized to the desired salt concentration
n_salt = int(c_salt * BOX_V.to('l') * MOLE)
print("initial n_salt:", n_salt)
system.part.add(pos=np.random.random((n_salt, 3)) * BOX_L_REDUCED,
                type=[TYPES["Na"]] * n_salt,
                q=[CHARGES["Na"]] * n_salt)
system.part.add(pos=np.random.random((n_salt, 3)) * BOX_L_REDUCED,
                type=[TYPES["Cl"]] * n_salt,
                q=[CHARGES["Cl"]] * n_salt)

print(f"The system was initialized with {len(system.part)} particles")


# ### Set up non-bonded-interactions
if USE_WCA:
    # set the WCA interaction betwee all particle pairs
    for type_1 in TYPES.values():
        for type_2 in TYPES.values():
            if type_1 >= type_2:
                system.non_bonded_inter[type_1, type_2].wca.set_params(epsilon=1.0, sigma=1.0)

    if(epsilon_reduced != 0.0):
        for type_1 in [TYPES['A'], TYPES['HA']]:
            for type_1 in [TYPES['A'], TYPES['HA']]:
                if type_1 >= type_2:
                    system.non_bonded_inter[type_1, type_2].lj.set_params(epsilon=epsilon, sigma=1.0, cut=2.5)
    # relax the overlaps with steepest descent
    print("steepest descent")
    system.integrator.set_steepest_descent(f_max=0, gamma=0.2, max_displacement=0.1)
    system.integrator.run(5000)
    print("velocity verlet")
    system.integrator.set_vv()  # to switch back to velocity Verlet
    system.integrator.run(20)
    system.integrator.set_steepest_descent(f_max=0, gamma=0.2, max_displacement=0.1)
    system.integrator.run(20)
    system.integrator.set_vv()  # to switch back to velocity Verlet
print("done velocity verlet")

# add thermostat and short integration to let the system relax further
system.thermostat.set_langevin(kT=KT_REDUCED, gamma=1.0, seed=7)
print("Langevin")
system.integrator.run(steps=1000)
print("done Langevin")

if USE_ELECTROSTATICS:
    COULOMB_PREFACTOR=BJERRUM_LENGTH_REDUCED * KT_REDUCED
    if USE_P3M:
        coulomb = espressomd.electrostatics.P3M(prefactor = COULOMB_PREFACTOR, 
                                                accuracy=1e-3)
    else:
        coulomb = espressomd.electrostatics.DH(prefactor = COULOMB_PREFACTOR, 
                                               kappa = KAPPA_REDUCED, 
                                               r_cut = 1. / KAPPA_REDUCED)
        
    system.actors.add(coulomb)
else:
    # this speeds up the simulation of dilute systems with small particle numbers
    system.cell_system.set_n_square()


# ### Set up the constant pH ensemble using the reaction ensemble module
exclusion_radius = PARTICLE_SIZE_REDUCED if USE_WCA else 0.0
RE = espressomd.reaction_ensemble.ReactionEnsemble(
    temperature=KT_REDUCED,
    exclusion_radius=exclusion_radius,
    seed=77
)
RE.set_non_interacting_type(len(TYPES)) # this parameter helps speed up the calculation in an interacting system (will be fixed in the next Espresso release)

RE.add_reaction(
    gamma=K_NaCl_reduced,
    reactant_types=[],
    reactant_coefficients=[],
    product_types=[TYPES["Na"], TYPES["Cl"]],
    product_coefficients=[1,1],
    default_charges={TYPES["Na"]: CHARGES["Na"],
                     TYPES["Cl"]: CHARGES["Cl"],
                     }
)

RE.add_reaction(
    gamma=K_HCl_reduced,
    reactant_types=[],
    reactant_coefficients=[],
    product_types=[TYPES["H"], TYPES["Cl"]],
    product_coefficients=[1,1],
    default_charges={TYPES["H"]: CHARGES["H"],
                     TYPES["Cl"]: CHARGES["Cl"],
                     }
)

RE.add_reaction(
    gamma=K_NaOH_reduced,
    reactant_types=[],
    reactant_coefficients=[],
    product_types=[TYPES["Na"], TYPES["OH"]],
    product_coefficients=[1,1],
    default_charges={TYPES["Na"]: CHARGES["Na"],
                     TYPES["OH"]: CHARGES["OH"],
                     }
)

RE.add_reaction(
    gamma=K_w_reduced,
    reactant_types=[],
    reactant_coefficients=[],
    product_types=[TYPES["H"], TYPES["OH"]],
    product_coefficients=[1,1],
    default_charges={TYPES["H"]: CHARGES["H"],
                     TYPES["OH"]: CHARGES["OH"],
                     }
)

if (not PURE_RESERVOIR):
    RE.add_reaction(
        gamma=K_A_reduced,
        reactant_types=[TYPES["HA"]],
        reactant_coefficients=[1],
        product_types=[TYPES["A"], TYPES["H"]],
        product_coefficients=[1,1],
        default_charges={TYPES["HA"]: CHARGES["HA"],
                         TYPES["A"]: CHARGES["A"],
                         TYPES["H"]: CHARGES["H"],
                         }
    )
    
    RE.add_reaction(
        gamma=K_AOH_reduced,
        reactant_types=[ TYPES["HA"], TYPES["OH"] ],
        reactant_coefficients=[1, 1],
        product_types=[ TYPES["A"] ],
        product_coefficients=[1],
        default_charges={TYPES["HA"]: CHARGES["HA"],
                         TYPES["A"]:  CHARGES["A"],
                         TYPES["OH"]: CHARGES["OH"],
                         }
    )
    
    RE.add_reaction(
        gamma=K_ANa_reduced,
        reactant_types=[TYPES["HA"]],
        reactant_coefficients=[1],
        product_types=[TYPES["A"], TYPES["Na"]],
        product_coefficients=[1,1],
        default_charges={TYPES["HA"]: CHARGES["HA"],
                         TYPES["A"]: CHARGES["A"],
                         TYPES["Na"]: CHARGES["Na"],
                         }
    )
    
    RE.add_reaction(
        gamma=K_ACl_reduced,
        reactant_types=[ TYPES["HA"], TYPES["Cl"] ],
        reactant_coefficients=[1, 1],
        product_types=[ TYPES["A"] ],
        product_coefficients=[1],
        default_charges={TYPES["HA"]: CHARGES["HA"],
                         TYPES["A"]:  CHARGES["A"],
                         TYPES["Cl"]: CHARGES["Cl"],
                         }
    )

def equilibrate_reaction(reaction_steps=1):
    RE.reaction(reaction_steps)

def calc_observables(output_file, system, observables):
    # FIXME there is a lot of code repetition in this function that could be avoided
    values = []
    if(PURE_RESERVOIR):
        N_A = 0
        N_HA = 0
        N_ACID = N_A + N_HA
        c_HA = (N_HA /MOLE / BOX_V) 
        c_A  = (N_A  /MOLE / BOX_V) 
        alpha = np.nan
    else:
        N_A = system.number_of_particles(type=TYPES["A"]) 
        N_HA = system.number_of_particles(type=TYPES["HA"]) 
        N_ACID = N_A + N_HA
        alpha = N_A / (N_ACID)
        c_HA = (N_HA /MOLE / BOX_V) 
        c_A  = (N_A  /MOLE / BOX_V) 

    N_Na = system.number_of_particles(type=TYPES["Na"]) 
    N_Cl = system.number_of_particles(type=TYPES["Cl"]) 
    N_H = system.number_of_particles(type=TYPES["H"]) 
    N_OH = system.number_of_particles(type=TYPES["OH"]) 
    c_Na = (N_Na /MOLE / BOX_V) 
    c_Cl = (N_Cl /MOLE / BOX_V) 
    c_H  = (N_H  /MOLE / BOX_V) 
    c_OH = (N_OH /MOLE / BOX_V) 
    Rg_list =  system.analysis.calc_rg(chain_start=0, number_of_chains=1, chain_length = N_ACID)  
    Rg_value = Rg_list[0]*ureg('reduced_length')
    result = {}
    for obs in observables:
        if obs == "time":
            output_file.write("{:.7g}  ".format(system.time))
        elif obs == "N_A":
            output_file.write("{:d}  ".format(N_A))
            result[obs] = N_A
        elif obs == "N_HA":
            output_file.write("{:d}  ".format(N_HA))
            result[obs] = N_HA
        elif obs == "N_Na":
            output_file.write("{:d}  ".format(N_Na))
            result[obs] = N_Na
        elif obs == "N_Cl":
            output_file.write("{:d}  ".format(N_Cl))
            result[obs] = N_Cl
        elif obs == "N_OH":
            output_file.write("{:d}  ".format(N_OH))
            result[obs] = N_OH
        elif obs == "N_H":
            output_file.write("{:d}  ".format(N_H))
            result[obs] = N_H
        elif obs == "c_A":
            output_file.write("{:4.3g}  ".format(c_A.to('mol/l').magnitude))
        elif obs == "c_HA":
            output_file.write("{:4.3g}  ".format(c_HA.to('mol/l').magnitude))
            result[obs] = c_HA
        elif obs == "c_Na":
            output_file.write("{:4.3g}  ".format(c_Na.to('mol/l').magnitude))
            result[obs] = c_Na
        elif obs == "c_Cl":
            output_file.write("{:4.3g}  ".format(c_Cl.to('mol/l').magnitude))
            result[obs] = c_Cl
        elif obs == "c_OH":
            output_file.write("{:4.3g}  ".format(c_OH.to('mol/l').magnitude))
            result[obs] = c_OH
        elif obs == "c_H":
            output_file.write("{:4.3g}  ".format(c_H.to('mol/l').magnitude))
            result[obs] = c_H
        elif obs == "alpha":
            output_file.write("{:.3f}  ".format(alpha))
            result[obs] = alpha
        elif obs == "BOX_V_dm3":
            output_file.write("{:.4g}  ".format( BOX_V.to('dm**3').magnitude) )
        elif obs == "Rg_nm":
            output_file.write("{:.4f}  ".format(Rg_value.to('nm').magnitude))
        else:
            raise ValueEror("Unknown observable " + obs + "\n")
    output_file.write("\n")
    output_file.flush()
    return result


def perform_sampling(output_file, observables, num_samples, reaction_steps, 
                     prob_integration=0.5, integration_steps=1000, ):
    alphas = [];
    obs_values = [];
    next_i = 0
    # calculate the observables before first sampling
    obs = calc_observables(
                output_file = output_file,
                system = system, 
                observables = observables, 
                )
    print(f"initial values: alpha {obs['alpha']:.3f}  n_A {obs['N_A']:d}  n_HA {obs['N_HA']:d}  Na {obs['N_Na']:.3g}  Cl {obs['N_Cl']:.3g}  H {obs['N_H']:.3g}  OH {obs['N_OH']:.3g}  c_Na {obs['c_Na'].to('mol/l'):.3g}  c_Cl {obs['c_Cl'].to('mol/l'):.3g}")
    print("perform {:d} simulation runs".format(num_samples))
    for i in range(num_samples):
        if USE_WCA and np.random.random() < prob_integration:
            system.integrator.run(integration_steps)
        # we should do at least one reaction attempt per reactive particle
        RE.reaction(reaction_steps)
        obs = calc_observables(
                output_file = output_file,
                system = system, 
                observables = observables, 
                )
#        current_n = system.number_of_particles(type=type_A) 
        alphas.append(obs['alpha'])
        if(i == next_i ):
            print(f"run {i:d} time {system.time:.3g} completed {i/num_samples*100:.2f}%  current values: alpha {obs['alpha']:.3f}  n_A {obs['N_A']:d}  n_HA {obs['N_HA']:d}  Na {obs['N_Na']:.3g}  Cl {obs['N_Cl']:.3g}  H {obs['N_H']:.3g}  OH {obs['N_OH']:.3g}  c_Na {obs['c_Na'].to('mol/l'):.3g}  c_Cl {obs['c_Cl'].to('mol/l'):.3g}")
            if(i==0):
                i=1;
                next_i = 1;
            else:
                next_i = next_i * 2
    return np.array(alphas)

# run a productive simulation and collect the data
start_time = time.time()
equilibrate_reaction(reaction_steps=N_ACID + 1)  # pre-equilibrate the reaction to the new pH value
    
if (not PURE_RESERVOIR):
    for part_id in range(0,N_ACID):
        if (part_id % CHAIN_LENGTH == 0):
            pass
        else:
            prev_p = part_id - 1
            current_p = system.part[part_id]
            current_p.delete_bond((eq_bond, prev_p))
            current_p.add_bond((final_bond, prev_p))

if(DEBUG):
    print_particles(system, exit = True)

output_file = open(sysname + ".csv", "w")
observables =  ["time", "N_A", "N_HA", "N_Na", "N_Cl", "N_OH", "N_H", "c_A", "c_HA", "c_Na", "c_Cl", "c_OH", "c_H", "alpha", "BOX_V_dm3", "Rg_nm"]
for obs in observables:
    output_file.write(obs + "  ")
output_file.write("\n")

# perform sampling
alphas = perform_sampling(output_file,
                 observables = observables,
                 num_samples=NUM_SAMPLES, 
                 reaction_steps=N_ACID + 1,
                 integration_steps = steps_per_round,
                 prob_integration=PROB_INTEGRATION)  # perform sampling / run production simulation
runtime = (time.time() - start_time) * ureg.s
mean_alpha = np.mean(alphas)
mean_n_A = N_ACID * mean_alpha
act_donnan_coefficient = calc_donnan_coefficient( mean_alpha*C_ACID.to('mol/l'), I_res.to('mol/l') )
ideal_alpha_with_donnan = calc_ideal_alpha(pH = pH+np.log10(act_donnan_coefficient), pKa = pKa)
print(f"runtime: {runtime:.2g}\n",
      f"measured number of A-: {mean_n_A:.2f}\n", 
      f"measured alpha: {mean_alpha:.2f}\n", 
      f"stdev: {np.std(alphas):.2f}\n", 
      f"ideal_alpha: {ideal_alpha}\n", 
      f"ideal_alpha with Donnan: {ideal_alpha_with_donnan}\n", 
      f"number of values: {alphas.shape}\n", 
      )
print("\nDONE")

sys.exit()
