import numpy as np
import pandas as pd

__all__ = ["get_dt", "new_index", "pretty_print_results"]

def get_params_from_file_name(filename):
    newcsvfile1 = filename.split("Seq-")[1]
    newcsvfile2 = newcsvfile1.split("_pH-")
    newcsvfile3 = newcsvfile2[1].split("_conc-")
    newcsvfile4 = newcsvfile3[1].split("_ionstr-")
    newcsvfile5 = newcsvfile4[1].split(".csv") [0]
    #print("params:", params)
    return {'sequence': newcsvfile2[0], 
            'pH': newcsvfile3[0], 
            'concentration': newcsvfile4[0], 
            'ionic strength': newcsvfile5}


def get_dt(data):
#    print("data:", data)
    time = data['time']
    imax = data.shape[0]
    dt_init = time[1] - time[0]
    warn_lines = [];
    for i in range(1,imax):
        dt = time[i] - time[i-1]
        #print("i, t, dt:", i, time[i], dt)
        if(np.abs((dt_init - dt)/dt) > 0.01 ):
            warn_lines.append("Row {} dt = {} = {} - {} not equal to dt_init = {}\n".format(i, dt, time[i], time[i-1], dt_init)
            )
    if(len(warn_lines) > 20):
        print("\n*** Warning: Data file may be broken!*** \n\nFound {} data rows with inconsistent Delta t:\n".format(len(warn_lines))
             )
        for line in warn_lines:
            print(line)
    return dt

def new_index(data_frame, suffix, skip = ['time','step']):
    new_index = []
    for index in data_frame.index:
        if(index in skip):
            new_index.append(index)
        else:
            new_index.append(index+suffix)
#    print("new_index:", new_index)
    return(new_index)

def pretty_print_results(results,filename):
    f = open(filename,"w")
    rjust_N = 4
    rjust_param = 8
    rjust_float = 9 
    fmt_N = '{:'+str(rjust_N)+'d}'
    fmt_obs = '{:'+str(rjust_float)+'.3g}'
    keys = results.keys()
    for key in keys:
        if(key=='N'):
            f.write(key.rjust(rjust_N))
        elif(key == 'Init' or key == 'Solvent'):
            f.write(key.rjust(rjust_param))
        else:
            f.write(key.rjust(rjust_float))                
    f.write('\n')
    for row_number in range(0,results.shape[0]):
        #print("row:", row_number)
        values = results.loc[row_number]
        #print("values:", values)
        for key in keys:
            if(key=='N'):
                f.write(fmt_N.format(values[key]))
            elif(key == 'Init' or key == 'Solvent'):
                f.write(values[key].rjust(rjust_param))
            else:
                f.write(fmt_obs.format(values[key]))                
        f.write('\n')
    f.close()
            
