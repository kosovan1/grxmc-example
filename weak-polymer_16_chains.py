"""
This sample performs a grand canonical simulation of a salt solution.
"""
#
# Copyright (C) 2013-2018 The ESPResSo project
#
# This file is part of ESPResSo.
#
# ESPResSo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ESPResSo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import print_function
import numpy as np
import scipy
import sys

import espressomd
from espressomd import code_info
from espressomd import analyze
from espressomd import integrate
from espressomd import reaction_ensemble
from espressomd import electrostatics
#from espressomd import diamond
from espressomd.interactions import *
from scipy import interpolate
from espressomd import minimize_energy
from espressomd.polymer import create_polymer
from espressomd.io.writer import vtf
from functools import reduce

from statistic import *
import csv

import gzip
import pickle
import os
import time

required_features = ["ELECTROSTATICS", "EXTERNAL_FORCES", "LENNARD_JONES"]
espressomd.assert_features(required_features)

# print help message if proper command-line arguments are not provided
if (len(sys.argv) != 8):
    print("\nGot ", str(len(sys.argv) - 1), " arguments, need 7\n\nusage:" +
          sys.argv[0] + " final_box_l MPC Kcideal_in_mol_per_l cs_bulkin_mol_per_l bjerrum pH run_id\n")
    sys.exit()

# System parameters
#############################################################


# example call pypresso weak-polymer.py 20 39 1e-4 0.001 2 1e-9 0 #Note that the scripts becomes more unstable as the number of monomers per chain become smaller (MPC)
final_box_l = float(sys.argv[1])  #e.g. 56
MPC = int(sys.argv[2]) #e.g. 50
Kcideal_in_mol_per_l = float(sys.argv[3])  #e.g. 10**-1.5, 10**-4
cs_bulk_in_mol_per_l = float(sys.argv[4])  #e.g. 0.1 mol/l
bjerrum = float(sys.argv[5])  #e.g. 2.0
pH = float(sys.argv[6])  #e.g. pH in bulk in mol/l from 1 to 13 works good
run_id = int(sys.argv[7]) #e.g. 0

# in 1/sigma**3, sigma=3.55Angstrom #0.00269541778/sigma**3=0.1 mol/l
conversion_factor_from_1_per_sigma_3_to_mol_per_l = 37.1
temperature = 1.0
cs_bulk = cs_bulk_in_mol_per_l/conversion_factor_from_1_per_sigma_3_to_mol_per_l

ionic_strength, excess_chemical_potential_monovalent_pairs_in_bulk_data, bjerrums, excess_chemical_potential_monovalent_pairs_in_bulk_data_error = np.loadtxt(
    "excess_chemical_potential.dat", unpack=True)  # remember, excess chemical potential does not know about types
excess_chemical_potential_monovalent_pairs_in_bulk = interpolate.interp1d(
    ionic_strength, excess_chemical_potential_monovalent_pairs_in_bulk_data)


# dimensionless dissociation constant Kw=relative_activity(H)*relative_activity(OH)
Kw = 10**-14
cref_in_mol_per_l = 1.0  # in mol/l


def calculate_concentrations_roots(cs_bulk, pH):
    """ calculate the ion concentrations agreeing with the excess chemical potentials, pH and cs_bulk """
    global temperature, Kw, cref_in_mol_per_l, conversion_factor_from_1_per_sigma_3_to_mol_per_l

    pH_pOH = np.array([pH, -np.log10(Kw) - pH])
    convert_c = conversion_factor_from_1_per_sigma_3_to_mol_per_l / cref_in_mol_per_l

    def c_all_ions_from_c_H_OH(c_H_OH):
        """ calculate all ion concentrations from the concentration of H+ and OH- """
        cH = c_H_OH[0]
        cOH = c_H_OH[1]
        cNa = cCl = cs_bulk
        if cOH > cH:
            cNa += cOH - cH
        else:
            cCl += cH - cOH
        return np.array([cH, cOH, cNa, cCl])

    def difference_function(c_H_OH):
        """difference between the determined and the target pH/pOH"""
        c_all = c_all_ions_from_c_H_OH(c_H_OH)
        ionic_strength = 0.5 * np.sum(c_all)
        determined_pH_pOH = -np.log10(
            c_all[0:2]
            * convert_c
            * np.exp((excess_chemical_potential_monovalent_pairs_in_bulk(ionic_strength)) / (2.0 * temperature))
        )
        return determined_pH_pOH - pH_pOH

    # initial guess of the concentrations
    c_H_OH_0 = np.power(10.0, -pH_pOH) / convert_c

    # optimize the concentrations
    c_H_OH_opt = scipy.optimize.fsolve(difference_function, c_H_OH_0)
    return c_all_ions_from_c_H_OH(c_H_OH_opt)


# calculate concentrations self consistently
[cH_bulk, cOH_bulk, cNa_bulk,
    cCl_bulk] = calculate_concentrations_roots(cs_bulk, pH)
ionic_strength_bulk = 0.5*np.sum([cH_bulk, cOH_bulk, cNa_bulk, cCl_bulk])
determined_pH = -np.log10(cH_bulk*conversion_factor_from_1_per_sigma_3_to_mol_per_l/cref_in_mol_per_l *
                          np.exp((excess_chemical_potential_monovalent_pairs_in_bulk(ionic_strength_bulk))/(2.0*temperature)))


def check_concentrations():
    if(abs(Kw-cOH_bulk*cH_bulk*np.exp((excess_chemical_potential_monovalent_pairs_in_bulk(ionic_strength_bulk))/temperature)*conversion_factor_from_1_per_sigma_3_to_mol_per_l**2/cref_in_mol_per_l**2) > 1e-14):
        raise RuntimeError("Kw incorrect")
    if(abs(cNa_bulk+cH_bulk-cOH_bulk-cCl_bulk) > 1e-14):
        raise RuntimeError("bulk is not electroneutral")
    if(abs(pH-determined_pH) > 1e-5):
        raise RuntimeError(
            "pH is not compatible with ionic strength and bulk H+ concentration")
    if(abs(cs_bulk-min(cNa_bulk, cCl_bulk)) > 1e-14):
        raise RuntimeError("bulk salt concentration is not correct")
    if(abs(pH-7) < 1e-14):
        if((cH_bulk/cOH_bulk-1) > 1e-5):
            raise RuntimeError("cH and cOH need to be symmetric at pH 7")


print("after self consistent concentration calculation: cH_bulk, cOH_bulk, cNa_bulk, cCl_bulk",
      cH_bulk, cOH_bulk, cNa_bulk, cCl_bulk)
print("check KW: ", Kw, cOH_bulk*cH_bulk*np.exp((excess_chemical_potential_monovalent_pairs_in_bulk(ionic_strength_bulk)
                                                 )/temperature)*conversion_factor_from_1_per_sigma_3_to_mol_per_l**2/cref_in_mol_per_l**2)
# note that charges are neutral up to numerical precision. femto molar charge inequalities are not important in the bulk.
print("check electro neutrality bulk after",
      cNa_bulk+cH_bulk-cOH_bulk-cCl_bulk)
print("check pH: input", pH, "determined pH", determined_pH)
print("check cs bulk: input", cs_bulk,
      "determinde cs_bulk", min(cNa_bulk, cCl_bulk))
print("check cH_bulk/cOH_bulk:", cH_bulk/cOH_bulk)
check_concentrations()

# Integration parameters
#############################################################
simulation_parameters = [final_box_l, MPC, Kcideal_in_mol_per_l, cs_bulk,
                         bjerrum, cH_bulk, cOH_bulk, cNa_bulk, cCl_bulk, ionic_strength_bulk]

system = espressomd.System(box_l=[final_box_l, final_box_l, final_box_l])
system.set_random_state_PRNG()
#system.seed = system.cell_system.get_state()['n_nodes'] * [1234]
np.random.seed(seed=np.random.randint(2**30-1))

system.time_step = 0.01
system.cell_system.skin = 0.4

system.thermostat.set_langevin(kT=temperature, gamma=1.0)
system.cell_system.max_num_cells = 2744


#############################################################
#  Setup System                                             #
#############################################################

# Particle setup
#############################################################
type_H = 0
type_A = 1
type_Cl = 2
type_Na = 3
type_constraint = 4
type_HA = 5
type_OH = 6
types = [type_H, type_A, type_Cl, type_Na, type_constraint, type_HA, type_OH]
charges_of_types = {type_H: 1, type_A: -1, type_Cl: -1, type_Na: 1}

lj_eps = 1.0
lj_sig = 1.0
lj_cut = 2**(1.0 / 6) * lj_sig
for type_1 in types:
    for type_2 in types:
        system.non_bonded_inter[type_1, type_2].lennard_jones.set_params(
            epsilon=lj_eps, sigma=lj_sig,
            cutoff=lj_cut, shift="auto")

# diamond uses bond with id 0
fene = FeneBond(k=30.0, d_r_max=1.5, r_0=0)
system.bonded_inter.add(fene)

# length for Kremer-Grest chain
bond_length = 0.9

# We can now call diamond to place the monomers, crosslinks and bonds.
number_of_chains = 16
create_polymer(MPC=MPC, N_P=number_of_chains, bond_length=bond_length,
               bond=fene, mode=2, shield=0.9, start_pos=[0, 0, 0])
system.part[:].type = type_HA
num_gel_particles = len(system.part[:].type)
gel_ids = range(0, num_gel_particles)
print("number of gel particles", num_gel_particles)
bonds_from = []
bonds_to = []
for i in range(num_gel_particles):
    for j in range(len(system.part[i].bonds)):
        bonds_from.append(i)
        bonds_to.append(system.part[i].bonds[j][1])


def calculate_average_bond_length(bonds_from, bonds_to):
    current_bond_lengths = []
    for i, j in zip(bonds_from, bonds_to):
        current_bond_lengths.append(np.sqrt(system.analysis.min_dist2(
            system.part[i].pos_folded, system.part[j].pos_folded)))
    return np.mean(current_bond_lengths)


outfile = open('diamond_before_warmup_'+str(final_box_l)+'.vtf', 'w')
vtf.writevsf(system, outfile)
vtf.writevcf(system, outfile)
outfile.close()

###########################################
# set up reactions

RE = reaction_ensemble.ReactionEnsemble(
    temperature=temperature, exclusion_radius=0.9)

# coupling to the reservoir
RE.add_reaction(gamma=cNa_bulk*cCl_bulk * np.exp(excess_chemical_potential_monovalent_pairs_in_bulk(ionic_strength_bulk) / temperature), reactant_types=[],
                reactant_coefficients=[], product_types=[type_Na, type_Cl], product_coefficients=[1, 1], default_charges={type_Na: 1, type_Cl: -1})
RE.add_reaction(gamma=cOH_bulk/cCl_bulk, reactant_types=[type_Cl], reactant_coefficients=[
                1], product_types=[type_OH], product_coefficients=[1], default_charges={type_OH: -1, type_Cl: -1})
RE.add_reaction(gamma=cH_bulk/cNa_bulk, reactant_types=[type_Na], reactant_coefficients=[
                1], product_types=[type_H], product_coefficients=[1], default_charges={type_Na: 1, type_H: 1})

# implementing the reactions
# HA <-> A- + H+
RE.add_reaction(gamma=Kcideal_in_mol_per_l/conversion_factor_from_1_per_sigma_3_to_mol_per_l, reactant_types=[type_HA], reactant_coefficients=[
                1], product_types=[type_A, type_H], product_coefficients=[1, 1], default_charges={type_HA: 0, type_H: 1, type_A: -1})
# HA+ OH- <-> A-
RE.add_reaction(gamma=(Kcideal_in_mol_per_l/Kw)*conversion_factor_from_1_per_sigma_3_to_mol_per_l, reactant_types=[type_HA, type_OH], reactant_coefficients=[
                1, 1], product_types=[type_A], product_coefficients=[1], default_charges={type_HA: 0, type_OH: -1, type_A: -1})  # cH*cA/cHA /(cOH*cH)=cA/(cOH*cHA)
# HA <-> A- +Na+
# Kcideal_in_mol_per_l*K_NaOH/Kw
K_NaOH = cNa_bulk*cOH_bulk * \
    np.exp(excess_chemical_potential_monovalent_pairs_in_bulk(
        ionic_strength_bulk) / temperature)
Kw_cref_squared_in_1_div_sigma_to_power_six = Kw*cref_in_mol_per_l**2 / \
    conversion_factor_from_1_per_sigma_3_to_mol_per_l**2
RE.add_reaction(gamma=Kcideal_in_mol_per_l/conversion_factor_from_1_per_sigma_3_to_mol_per_l*(K_NaOH/Kw_cref_squared_in_1_div_sigma_to_power_six), reactant_types=[
                type_HA], reactant_coefficients=[1], product_types=[type_A, type_Na], product_coefficients=[1, 1], default_charges={type_HA: 0, type_Na: 1, type_A: -1})
# HA + Cl- <-> A-
# Kcideal_in_mol_per_l/K_HCl
K_HCl = cH_bulk*cCl_bulk * \
    np.exp(excess_chemical_potential_monovalent_pairs_in_bulk(
        ionic_strength_bulk) / temperature)
RE.add_reaction(gamma=Kcideal_in_mol_per_l/conversion_factor_from_1_per_sigma_3_to_mol_per_l*1/K_HCl, reactant_types=[
                type_HA, type_Cl], reactant_coefficients=[1, 1], product_types=[type_A], product_coefficients=[1], default_charges={type_HA: 0, type_Cl: -1, type_A: -1})


RE.set_volume(np.prod(system.box_l))
print(RE.get_status())


def MC_swap_A_HA_particles(type_HA, type_A):
    As = system.part.select(type=type_A)
    HAs = system.part.select(type=type_HA)
    ids_A = As.id
    ids_HA = HAs.id
    if(len(ids_A) > 0 and len(ids_HA) > 0):
        # choose random_id_A, choose_random_id_HA
        random_id_A = ids_A[np.random.randint(0, len(ids_A))]
        random_id_HA = ids_HA[np.random.randint(0, len(ids_HA))]

        old_energy = system.analysis.energy()["total"]
        # adapt type and charge
        system.part[random_id_A].type = type_HA
        system.part[random_id_A].q = 0
        system.part[random_id_HA].type = type_A
        system.part[random_id_HA].q = -1
        new_energy = system.analysis.energy()["total"]
        # apply metropolis criterion, accept or reject based on energetic change
        if(np.random.random() < min(1, np.exp(-(new_energy-old_energy)/temperature))):
            # accept
            pass
        else:
            # reject
            system.part[random_id_A].type = type_A
            system.part[random_id_A].q = -1
            system.part[random_id_HA].type = type_HA
            system.part[random_id_HA].q = 0


def reaction(steps):
    global type_HA, type_A, MPC
    RE.reaction(steps)
    for a in range(int(MPC/10.0)):
        MC_swap_A_HA_particles(type_HA, type_A)


reaction(30000+MPC)


def create_particle_in_safe_dist(part_type, part_charge):
    min_dist = 2**(1.0/6.0)  # in units of sigma
    # add particle after last known particle
    pos = system.box_l[0]*np.random.random(3)
    part = system.part.add(pos=pos, type=part_type, q=part_charge)
    while(system.analysis.dist_to(id=part.id) < min_dist*1.1):
        pos = system.box_l[0]*np.random.random(3)
        system.part[part.id].pos = pos
    return part.id


# add salt for p3m tuning, this is removed automatically by grandcanonical moves if the salt needs to be removed
for i in range(MPC+int(0.5*cs_bulk*final_box_l**3)):
    create_particle_in_safe_dist(type_Cl, -1)
    create_particle_in_safe_dist(type_Na, 1)

tuned_skin = system.cell_system.tune_skin(
    min_skin=1.0, max_skin=1.6, tol=0.05, int_steps=200)
print("tuned_skin", tuned_skin)

system.integrator.run(steps=1000)

p3m = electrostatics.P3M(prefactor=bjerrum*temperature, accuracy=1e-3)
system.actors.add(p3m)
p3m_params = p3m.get_params()
for key in list(p3m_params.keys()):
    print("{} = {}".format(key, p3m_params[key]))

tuned_skin = system.cell_system.tune_skin(
    min_skin=1.0, max_skin=1.6, tol=0.05, int_steps=200)
print("tuned_skin", tuned_skin)

for j in range(10):
    print("reaction batch", j)
    RE.reaction(9000)

# Warmup
#############################################################
# warmup integration (with capped LJ potential)
warm_n_times = 100  # XXX 100
# do the warmup until the particles have at least the distance min_dist
# set LJ cap
lj_cap = 100
system.force_cap = lj_cap
system.time_step = 0.01

# Warmup Integration Loop
i = 0

start_warmup = time.clock()
while (i < warm_n_times):
    print(i, "warmup")
    reaction(MPC*20)
    system.integrator.run(steps=1000+MPC*2)
    print("mindist", system.analysis.min_dist(),
          "N_tot", len(system.part[:].q))
    i += 1
    # Increase LJ cap
    lj_cap = lj_cap + 10
    system.force_cap = lj_cap
end_warmup = time.clock()
elapsed_time_in_minutes = (end_warmup - start_warmup)/60.0  # in seconds
time_per_cyle = elapsed_time_in_minutes/warm_n_times  # in minutes/cycle
# minus 5 minute to make sure the last checkpoint is written before the time slot on bee ends
nr_of_cylces_in_15_minutes = int((15.-5)/time_per_cyle)
print("nr_of_cylces_in_15_minutes", nr_of_cylces_in_15_minutes)

# remove force capping
system.force_cap = 0
system.time_step = 0.01
# MC warmup
reaction(5*(MPC*16+20))
RE.reaction(90000)

outfile = open('diamond_after_warmup_'+str(final_box_l)+'.vtf', 'w')
vtf.writevsf(system, outfile)
vtf.writevcf(system, outfile)
outfile.close()

squared_end_to_end_distances = []
num_Hs = []
num_OHs = []
num_Nas = []
num_Cls = []
num_As = []
total_isotropic_pressures = []
squared_end_to_end_distances = []
squared_radii_of_gyration = []
total_stress_tensors = []
kinetic_stress_tensors = []
coulomb_stress_tensors = []
bonded_stress_tensors = []
nonbonded_stress_tensors = []
bond_lengths = []
gel_charges = []

if(os.path.exists("checkpoint_"+str(final_box_l)+".pgz")):
    # try to load safed statistical data
    try:
        with gzip.GzipFile("checkpoint_"+str(final_box_l)+".pgz", 'rb') as fcheck:
            print("loading checkpoint")
            data = pickle.load(fcheck)
            num_Hs = data[0]
            num_OHs = data[1]
            num_Nas = data[2]
            num_Cls = data[3]
            num_As = data[4]
            total_isotropic_pressures = data[5]
            stress_tensor = data[6]
            total_stress_tensors = data[7]
            kinetic_stress_tensors = data[8]
            coulomb_stress_tensors = data[9]
            bonded_stress_tensors = data[10]
            nonbonded_stress_tensors = data[11]
            gel_charges = data[12]
            squared_end_to_end_distances = data[13]
            squared_radii_of_gyration = data[14]
            bond_lengths = data[15]
    except:
        pass

filename = "observables_run_"+str(run_id)+"box_l_"+str(final_box_l)
np.savetxt(filename+"_bonds.out", np.c_[bonds_from, bonds_to])

scalar_observables = [num_Hs, num_OHs, num_Nas, num_Cls, num_As, total_isotropic_pressures,
                      squared_end_to_end_distances, squared_radii_of_gyration, bond_lengths]  # collect references to scalar observables

box_l = final_box_l
volume = final_box_l**3
for i in range(10000):
    N_steps = len(num_Hs)+1
    reaction(MPC*20)
    system.integrator.run(steps=1000+MPC*2)  # XXX 1000
    num_Hs.append(system.number_of_particles(type=type_H))
    num_OHs.append(system.number_of_particles(type=type_OH))
    num_Nas.append(system.number_of_particles(type=type_Na))
    num_Cls.append(system.number_of_particles(type=type_Cl))
    num_As.append(system.number_of_particles(type=type_A))
    total_isotropic_pressures.append(system.analysis.pressure()["total"])
    stress_tensor = system.analysis.stress_tensor()
    total_stress_tensors.append(stress_tensor["total"])
    kinetic_stress_tensors.append(stress_tensor["kinetic"])
    coulomb_stress_tensors.append(stress_tensor["coulomb"])
    bonded_stress_tensors.append(stress_tensor["bonded"])
    nonbonded_stress_tensors.append(stress_tensor["non_bonded"])

    gel_charges.append(system.part[0:num_gel_particles].q)
    squared_end_to_end_distances.append(system.analysis.calc_re(
        chain_start=0, number_of_chains=number_of_chains, chain_length=MPC)[2])
    squared_radii_of_gyration.append(system.analysis.calc_rg(
        chain_start=0, number_of_chains=number_of_chains, chain_length=MPC)[2])

    bond_lengths.append(calculate_average_bond_length(bonds_from, bonds_to))

    if(i % nr_of_cylces_in_15_minutes == 0):

        scalar_observables_means = []
        scalar_observables_errors = []
        for scalar_observable_i in range(len(scalar_observables)):
            mean_scalar_observable, correlation_corrected_error = calc_error(
                scalar_observables[scalar_observable_i])
            scalar_observables_means.append(mean_scalar_observable)
            scalar_observables_errors.append(correlation_corrected_error)

        # save degree_of_dissociation
        degree_of_dissociation = scalar_observables_means[4]/(
            number_of_chains*MPC)
        err_degree_of_dissociation = scalar_observables_errors[4]/(
            number_of_chains*MPC)
        scalar_observables_means.append(degree_of_dissociation)
        scalar_observables_errors.append(err_degree_of_dissociation)

        output_list = simulation_parameters + \
            [N_steps]+scalar_observables_means+scalar_observables_errors

        with open(filename+"_pressure.out", mode="w") as f:
            writer = csv.writer(f)
            header = "# final_box_l,MPC,Kcideal_in_mol_per_l, cs_bulk, bjerrum, cH_bulk, cOH_bulk, cNa_bulk, cCl_bulk, ionic_strength_bulk, Nsteps , num_Hs, num_OHs, num_Nas, num_Cls, num_As, total_isotropic_pressures, squared_end_to_end_distances, squared_radii_of_gyration, bond_lengths, degree_of_dissociation, err_num_Hs, err_num_OHs, err_num_Nas, err_num_Cls, err_num_As, err_total_isotropic_pressures, err_squared_end_to_end_distances, err_squared_radii_of_gyration, err_bond_lengths, err_degree_of_dissociation "
            writer.writerow(header.split(','))
            writer.writerow(output_list)
        print("N_tot", len(system.part[:].q), "HA", system.number_of_particles(type=type_HA), "A-", system.number_of_particles(type=type_A), "H+", system.number_of_particles(
            type=type_H), "total charge", np.sum(system.part[:].q), "mean squared end_to_end_distance", np.mean(squared_end_to_end_distances))

        # save tensorial observables
        mean_total_stress_tensor = np.mean(total_stress_tensors, axis=0)
        std_error_mean_total_stress_tensor = np.std(
            total_stress_tensors, axis=0, ddof=1)/np.sqrt(N_steps)
        np.savetxt(filename+str("_total_stress.out"), mean_total_stress_tensor)
        np.savetxt(filename+str("_total_stress_err.out"),
                   std_error_mean_total_stress_tensor)
        mean_kinetic_stress_tensor = np.mean(kinetic_stress_tensors, axis=0)
        std_error_mean_kinetic_stress_tensor = np.std(
            kinetic_stress_tensors, axis=0, ddof=1)/np.sqrt(N_steps)
        np.savetxt(filename+str("_kinetic_stress.out"),
                   mean_kinetic_stress_tensor)
        np.savetxt(filename+str("_kinetic_stress_err.out"),
                   std_error_mean_kinetic_stress_tensor)
        mean_coulomb_stress_tensor = np.mean(coulomb_stress_tensors, axis=0)
        std_error_mean_coulomb_stress_tensor = np.std(
            coulomb_stress_tensors, axis=0, ddof=1)/np.sqrt(N_steps)
        np.savetxt(filename+str("_coulomb_stress.out"),
                   mean_coulomb_stress_tensor)
        np.savetxt(filename+str("_coulomb_stress_err.out"),
                   std_error_mean_coulomb_stress_tensor)
        mean_bonded_stress_tensor = np.mean(bonded_stress_tensors, axis=0)
        std_error_mean_bonded_stress_tensor = np.std(
            bonded_stress_tensors, axis=0, ddof=1)/np.sqrt(N_steps)
        np.savetxt(filename+str("_bonded_stress.out"),
                   mean_bonded_stress_tensor)
        np.savetxt(filename+str("_bonded_stress_err.out"),
                   std_error_mean_bonded_stress_tensor)
        mean_nonbonded_stress_tensor = np.mean(
            nonbonded_stress_tensors, axis=0)
        std_error_mean_nonbonded_stress_tensor = np.std(
            nonbonded_stress_tensors, axis=0, ddof=1)/np.sqrt(N_steps)
        np.savetxt(filename+str("_nonbonded_stress.out"),
                   mean_nonbonded_stress_tensor)
        np.savetxt(filename+str("_nonbonded_stress_err.out"),
                   std_error_mean_nonbonded_stress_tensor)

        # save gel charges
        mean_gel_charges = np.mean(gel_charges, axis=0)
        std_error_gel_charges = np.std(
            gel_charges, axis=0, ddof=1)/np.sqrt(N_steps)
        np.savetxt(filename+"_gel_charges.out", np.c_[gel_ids, mean_gel_charges,
                                                      std_error_gel_charges], header="id, average_charge, std_error_mean_charge")
        np.savetxt(filename+"_acceptance_rates.out", np.c_[RE.get_acceptance_rate_reaction(0), RE.get_acceptance_rate_reaction(1), RE.get_acceptance_rate_reaction(2), RE.get_acceptance_rate_reaction(3), RE.get_acceptance_rate_reaction(4), RE.get_acceptance_rate_reaction(
            5), RE.get_acceptance_rate_reaction(6), RE.get_acceptance_rate_reaction(7), RE.get_acceptance_rate_reaction(8), RE.get_acceptance_rate_reaction(9), RE.get_acceptance_rate_reaction(10), RE.get_acceptance_rate_reaction(11), RE.get_acceptance_rate_reaction(12), RE.get_acceptance_rate_reaction(13)])

        outfile = open('diamond_sim_'+str(final_box_l)+'.vtf', 'w')
        vtf.writevsf(system, outfile)
        vtf.writevcf(system, outfile)
        outfile.close()
        with gzip.GzipFile("checkpoint_"+str(final_box_l)+".pgz", 'wb') as fcheck:
            print("writing checkpoint")
            data = [num_Hs, num_OHs, num_Nas, num_Cls, num_As, total_isotropic_pressures, stress_tensor, total_stress_tensors, kinetic_stress_tensors,
                    coulomb_stress_tensors, bonded_stress_tensors, nonbonded_stress_tensors, gel_charges, squared_end_to_end_distances, squared_radii_of_gyration, bond_lengths]
            pickle.dump(data, fcheck)
